<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsheader = new FieldsBuilder('header_options');

$optionsheader

->setLocation('options_page', '==', 'theme-header-settings')
	//Logo checkbox
	->addTrueFalse('add_logo')

    //Logo 
	->addImage('logo', [
		'label' => 'Logo',
		'ui' => $config->ui
	])
    ->conditional('add_logo', '==', 1)

    //Mobile Menu Logo checkbox
	->addTrueFalse('add_menulogo', [
		'label' => 'Add Logo to Mobile Menu'
    ])

    //Mobile Menu Logo 
	->addImage('menulogo', [
		'label' => 'Mobile Menu Logo',
		'ui' => $config->ui
	])
    ->conditional('add_menulogo', '==', 1);
    
return $optionsheader;