<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 50],
];

$wildcard = new FieldsBuilder('wildcard');

$wildcard
    ->addTab('settings', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.add_class'))
        ->addFields(get_field_partial('partials.module_title'))
        ->addFields(get_field_partial('partials.grid_options'));


$wildcard
    ->addTab('content', ['placement' => 'left'])
    //Repeater
    ->addRepeater('deck', [
      'min' => 1,
      'max' => 10,
      'button_label' => 'Add Card',
      'layout' => 'block',
    ])

    ->addFlexibleContent('cards', ['button_label' => 'Add Pieces'])
        //Pre Header
        ->addLayout('preheader')
            ->addText('preheader')
        // Header
        ->addLayout('header')
            ->addText('header')
        // WYSIWYG
        ->addLayout('wysiwyg')
            ->addWysiwyg('paragraph', [
                'label' => 'Wysiwyg',
            ])
                
        // Image
        ->addLayout('image')
            ->addImage('image')
        //Button
        ->addLayout('button')
            ->addFields(get_field_partial('modules.button'))
        //Code Area
        ->addLayout('code_area')
            ->addTextarea('code', [
                'label' => 'Code Area',
            ])
        //Card List
        ->addLayout('card list')
            ->addFields(get_field_partial('modules.card_list'))
        //Related Posts
        ->addLayout('related posts')
            ->addRelationship('article', [
                'label' => 'Article Picker',
                'post_type' => 'post',
                'min' => 4,
                'max' => 4,
            ])
        //Search
         ->addLayout('search')    
        //Google Map
        ->addLayout('google map')
            ->addGoogleMap('google_map')
        //Accordion
        ->addLayout('accordion')
            ->addRepeater('accordion_items', [
                'label' => 'Accordion Items',
                'min' => 1,
                'max' => 10,
                'button_label' => 'Add Accodion item',
                'layout' => 'block',
                ])
            ->addText('title', [
                'label' => 'Accordion Title'
            ])
            ->addWysiwyg('content', [
                'label' => 'Accordion Content'
            ])
        //Accreditations
        ->addLayout('accreditations')
            ->addGroup('accreditation_schema', [
                'label' => 'Accreditations'
            ])
                ->addTrueFalse('accreditation_box', [
                        'label' => 'Add Logos',
                        'wrapper' => ['width' => 10]
                    ])
                    ->addCheckbox('accreditations', [
                        'layout' => 'vertical',
                        'toggle' => 1,
                        'wrapper' => ['width' => 90]
                    ])
                    ->addChoices(
                        ['naatp' => 'NAATP'],
                        ['carf' => 'CARF'],
                        ['joint_commission' => 'Joint Commission'],
                        ['legitscript' => 'LegitScript']
                    )
                    ->conditional('accreditation_box', '==', 1 );   

return $wildcard;
      	