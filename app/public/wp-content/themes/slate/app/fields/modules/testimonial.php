<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$testimonial = new FieldsBuilder('testimonial');

$testimonial
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$testimonial
	->addTab('content', ['placement' => 'left'])
		// Card
		->addFields(get_field_partial('modules.card'))

    	//Repeater
		->addRepeater('testimonial', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Testimonial',
		  'layout' => 'block',
		  'wrapper' => [
	          'class' => 'deck',
	        ],
		])

		// Name
		->addText('name', [
			'label' => 'Name',
			'ui' => $config->ui
		])

		// Relationship
		->addText('relation', [
			'label' => 'Relationship',
			'ui' => $config->ui
		])
		->setInstructions('Example: Former Patient, Loved One, Referral Partner')

		// WYSIWYG
		->addWysiwyg('quote', [
			'label' => 'Quote',
			'ui' => $config->ui
		]);
	  	

return $testimonial;