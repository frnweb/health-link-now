<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$staff = new FieldsBuilder('staff');

$staff
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$staff
	->addTab('content', ['placement' => 'left'])

	// Header
	->addTrueFalse('check_box', [
			'label' => 'Add Header',
			'wrapper' => ['width' => 30]
		])
		->setInstructions('Optional Header')
		
		->addText('header', [
			'label' => 'Header',
			'wrapper' => ['width' => 70]
		])
		->conditional('check_box', '==', 1 )

	//Staff Deck
    ->addRelationship('staff', [
        'label' => 'Staff Members',
        'post_type' => 'sl_staff_cpts',
        'min' => 1,
    ])
    ->setInstructions('Choose multiple staff members to highlight');
    

return $staff;