<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$staff = new FieldsBuilder('staff_fields');

$staff
    ->setLocation('post_type', '==', 'sl_staff_cpts');
  
$staff
	->addText('first_name', [
		'label' => 'First Name'
	])
	->addText('last_name', [
		'label' => 'Last Name'
	])
	->addText('certifications', [
		'label' => 'Certifications'
	])
	->addText('staff_title', [
		'label' => 'Title'
	])
	->addText('staff_location', [
		'label' => 'Licensed Locations'
	]);

return $staff;