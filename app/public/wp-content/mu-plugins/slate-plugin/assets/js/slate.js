jQuery(document).ready(function( $ ) {

	// Register the theme uri 
	var $template = template.page_template;
	$template = $template.replace(".php", "");
	var $body = $( 'body' );

	$body.addClass( 'slate-admin-theme' );
	$body.addClass( 'has-' + $template );

});
// START ADMIN WORDPRESS ACF
(function($) {
/*
*  @author		uhswebteam
*  @details		function(s) to move/replace certain elements on the admin side of wordpress
*				to make the user interface for ACF easier to use.
*/
	
 $(document).ready(function(){
   "use strict";
 	
   var handle = $('.acf-repeater .acf-row-handle.order');
   var plus = '<a class="acf-icon -plus small acf-js-tooltip" href="#" data-event="add-row" title="Add row"></a>';
   var duplicate = '<a class="acf-icon -duplicate small acf-js-tooltip" href="#" data-event="duplicate-row" title="Duplicate row"></a>';
   var minus = '<a class="acf-icon -minus small acf-js-tooltip" href="#" data-event="remove-row" title="Remove row"></a>';
   
       $( plus ).appendTo( handle );
       $( duplicate ).appendTo( handle );
       $( minus ).appendTo( handle );

   $('.acf-repeater .acf-row-handle.remove').remove();
 });

$(document).on('click', '.acf-repeater .acf-row-handle.order', function(event) {
  if (!$(event.target).is('a[data-event="collapse-row"]')) {
        $(this).find('a[data-event="collapse-row"]').trigger('click');
  }
  event.preventDefault();
});

})( jQuery );

