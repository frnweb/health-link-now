<?php
// Youtube Embed Ifram
	function sl_embed( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'src'   => ''
        ), $atts );
        return '<iframe width="560" height="315" src="" data-src=' . esc_attr($specs['src'] ) . ' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	}
	add_shortcode ('embed', 'sl_embed' );
///EMBED
?>