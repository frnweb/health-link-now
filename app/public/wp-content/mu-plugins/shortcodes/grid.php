<?php
// ROW
	function sl_row ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> '',
		), $atts );
		if(in_array('equalizer', $specs, true)){
			$row = '<div class="sl_row sl_row--shortcode ' . esc_attr($specs['class'] ) . '" data-equalizer data-equalize-on="medium">' . do_shortcode(shortcode_unautop( $content )) . '</div>';
		}else{
			$row = '<div class="sl_row sl_row--shortcode ' . esc_attr($specs['class'] ) . '">' . do_shortcode(shortcode_unautop( $content )) . '</div>';
		}
		return $row;
	}
	add_shortcode ('row', 'sl_row' );
	add_shortcode ('row-nested', 'sl_row' );
///ROW

// COLUMN
	function sl_cell ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'small'		=> '',
			'medium'	=> '',
			'large'		=> '12',
			), $atts );
		return '<div class="small-' . esc_attr($specs['small']) . ' medium-' . esc_attr($specs['medium']) . ' large-' . esc_attr($specs['large']) . ' sl_cell">' . do_shortcode(shortcode_unautop( $content )) . '</div>';
	}
	add_shortcode ('col', 'sl_cell' );
	add_shortcode ('col-nested', 'sl_cell' );
///COLUMN
?>