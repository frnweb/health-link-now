<?php
/**
 * Plugin Name: FAQs CPTS Plugin
 * Description: This is the Custom Post Type for FRN FAQs.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_faqs_cpts() {

	$labels = array(
		'name'                  => _x( 'FAQs', 'Post Type General Name', 'sl_faqs_cpts' ),
		'singular_name'         => _x( 'FAQs', 'Post Type Singular Name', 'sl_faqs_cpts' ),
		'menu_name'             => __( 'FAQs', 'sl_faqs_cpts' ),
		'name_admin_bar'        => __( 'FAQs', 'sl_faqs_cpts' ),
		'archives'              => __( 'FAQ Archives', 'sl_faqs_cpts' ),
		'attributes'            => __( 'FAQ Attributes', 'sl_faqs_cpts' ),
		'parent_item_colon'     => __( 'Parent FAQ:', 'sl_faqs_cpts' ),
		'all_items'             => __( 'All FAQ', 'sl_faqs_cpts' ),
		'add_new_item'          => __( 'Add New FAQ', 'sl_faqs_cpts' ),
		'add_new'               => __( 'Add New', 'sl_faqs_cpts' ),
		'new_item'              => __( 'New FAQ', 'sl_faqs_cpts' ),
		'edit_item'             => __( 'Edit FAQs', 'sl_faqs_cpts' ),
		'update_item'           => __( 'Update FAQs', 'sl_faqs_cpts' ),
		'view_item'             => __( 'View FAQs', 'sl_faqs_cpts' ),
		'view_items'            => __( 'View FAQs', 'sl_faqs_cpts' ),
		'search_items'          => __( 'Search FAQ', 'sl_faqs_cpts' ),
		'not_found'             => __( 'Not found', 'sl_faqs_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_faqs_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_faqs_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_faqs_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_faqs_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_faqs_cpts' ),
		'insert_into_item'      => __( 'Insert into Locations', 'sl_faqs_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this FAQs', 'sl_faqs_cpts' ),
		'items_list'            => __( 'FAQs list', 'sl_faqs_cpts' ),
		'items_list_navigation' => __( 'FAQs list navigation', 'sl_faqs_cpts' ),
		'filter_items_list'     => __( 'Filter FAQs list', 'sl_faqs_cpts' ),
	);
	$args = array(
		'label'                 => __( 'FAQs', 'sl_faqs_cpts' ),
		'description'           => __( 'Custom Post Type for FRN FAQs', 'sl_faqs_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'faq','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-status',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_faqs_cpts', $args );

}
add_action( 'init', 'sl_faqs_cpts', 0 );

function sl_faqs_cpts_taxonomy() {
 
	$labels = array(
	  'name' 						=> _x( 'Categories', 'taxonomy general name' ),
	  'singular_name' 				=> _x( 'Category', 'taxonomy singular name' ),
	  'search_items' 				=>  __( 'Search Categories' ),
	  'all_items' 					=> __( 'All Categories' ),
	  'parent_item' 				=> __( 'Parent Category' ),
	  'parent_item_colon' 			=> __( 'Parent Category:' ),
	  'edit_item' 					=> __( 'Edit Category' ), 
	  'update_item' 				=> __( 'Update Category' ),
	  'add_new_item' 				=> __( 'Add New Category' ),
	  'new_item_name' 				=> __( 'New Category Name' ),
	  'menu_name' 					=> __( 'Categories' ),
	); 	

register_taxonomy('faq_categories',array('sl_faqs_cpts'), array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'show_admin_column' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'category' ),
  ));
}

add_action( 'init', 'sl_faqs_cpts_taxonomy', 0 );