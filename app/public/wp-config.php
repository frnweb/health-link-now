<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3VWFrMwwPjltT84IHSrC0orjk1PoMKlvefK5cuoETJ7en7SCPvqz336vpSyfj4Yfr5j+ybmEgkOtAxJop0FH7w==');
define('SECURE_AUTH_KEY',  'GwZjSWNi3IGB0bSdtpx2PgY+hfvx36dENle8UvyfFlSNPX04TTpeCXG9vRy2DYMjDsVtSzMmKM8tdk6Vn21z7Q==');
define('LOGGED_IN_KEY',    'canPqBmsatNJNH0VA2pXqMHrX1wAVIvKcrgXToGTO1tyr41bBwduRQ/rdN+tZBuMoL7lDK7Ugo5KrNfLosaAjA==');
define('NONCE_KEY',        'qKbbVzgPo6kRAZd/vFG3g4lr9K+0V23agSQy0dgrPT4YqP4xRy8f1GB3Mp5izNfUIkjdywwL/as6TyyE1I5dRw==');
define('AUTH_SALT',        'fy7Ys82S3QUGfC04lFDly52pDZ0cMLu0Pdhs1KsO2/6uq+Jy6LqB0s3g98znka8NU3aWyLnnwbZLY4sUu0ZVyA==');
define('SECURE_AUTH_SALT', 'E4sRHUyV8f/jzFSanGQIzrdWf00OyXGpGDkPyrRuiqLUkYuKB26bVpU2s4evzg4jyqNUqNUNPVdMtVVYVrI2Vw==');
define('LOGGED_IN_SALT',   'Aw+0poQHPxuVfEIS4QlfXb5nN7oDvT1tj45K+700BP7ewCjonQVkWw077MCD1jbTftmgVs5X8sc+3PqqgAnzrw==');
define('NONCE_SALT',       'i7zeGjLvi0i/yGT8/ED5M3+pE2rGLu0b7sHfXlEHJes85BlYOzX64mc/3ohCXMKcnPMoSb/sq5RsjcH1u3OGmw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
